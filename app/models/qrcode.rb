class Qrcode < ActiveRecord::Base
	has_attached_file :qrcode
	validates_attachment :qrcode, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
	before_validation :set_qrcode
	def set_qrcode
		if qr_img_url.present?
			image_data = qr_img_url[qr_img_url.index("base64,") + "base64,".size, qr_img_url.size]
	    StringIO.open(Base64.decode64(image_data)) do |str|
	      decorate_str(str)
	      str.original_filename = "#{Time.now.to_i}.jpg"
	      # str.content_type = "image/png"
	      self.qrcode = str
	      self.qrcode_content_type = "image/png"
	    end
	  end
  end
  def decorate_str(str)
    str.class.class_eval { attr_accessor :original_filename, :content_type }
  end
end
