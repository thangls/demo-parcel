class Image < ActiveRecord::Base

  include Rails.application.routes.url_helpers
  has_attached_file :image, styles: {
      thumb: ['200x200#', :jpeg],
      original: ['1920x1200>', :jpeg]
    },
    convert_options: { original: '-strip -auto-orient -colorspace sRGB' }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  belongs_to :user
  belongs_to :post
  def to_json
    {
      files: [
          {
            id: self.id,
            name: read_attribute(:image_file_name),
            size: read_attribute(:image_file_size),
            url: image.url(:thumb),
            delete_url: Rails.application.routes.url_helpers.image_path(self),
            delete_type: "DELETE"
          }
        ]
    }
  end

  attr_reader :image_remote_url

  def image_remote_url=(url_value)
    self.image = URI.parse(url_value)
    @image_remote_url = url_value
  end
end
