require 'typhoeus/adapters/faraday'
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # include Elasticsearch::Model
  # include Elasticsearch::Model::Callbacks

  # mappings dynamic: 'false' do
  #  indexes :email, type: 'string'
  # end
  # has_and_belongs_to_many :skills
  # searchable do
  #   text :email, :settings
  # end

end
