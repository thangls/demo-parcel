class Video < ActiveRecord::Base
	has_attached_file :video,
		:s3_protocol => :https,
		:path => "/public/:class/:attachment/:id_partition/:attachment_:id_:hash_:style.:extension",
		hash_secret: "secret",
		styles: {
        # :medium => {
        #   :geometry => "640x480",
        #   :format => 'mp4'
        # },
        :thumb => { :geometry => "160x120", :format => 'jpeg', :time => 15}
    }, :processors => [:transcoder]
	validates_attachment_content_type :video, content_type: /\Avideo\/.*\Z/
end
