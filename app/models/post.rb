class Post < ActiveRecord::Base
  validates :content, presence: true
  after_save :extract_url
  has_many :images
  def extract_url
    ExtractPostUrlJob.perform_later(self)
  end
end
