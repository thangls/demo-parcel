class Job < ActiveRecord::Base
  has_many :jobs_details
  belongs_to :category
  validates :title, :description, presence: true
  belongs_to :country
end
