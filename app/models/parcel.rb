class Parcel < ActiveRecord::Base
	enum status: [ :received, :sent ]
end
