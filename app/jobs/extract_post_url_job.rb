class ExtractPostUrlJob < ActiveJob::Base
  queue_as :default

  def perform(post)
    # Do something later
    url = detect_urls_from_content(post.content).first
    object = LinkThumbnailer.generate(url)
    post.update_column(:url_extract, object.to_json)
  rescue LinkThumbnailer::Exceptions => e
    Rails.logger.error(e.message)
  end

  def detect_urls_from_content(str)
    URI.extract(str, /http(s)?/)
  end
end