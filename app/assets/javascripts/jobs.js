jQuery(document).ready(function($) {
  $("#job_category_id").change(function(){
    $("#job_skill_needs_").multipleSelect("disable");
    $.ajax({
      url: "/jobs/get_processes",
      method: "GET",
      dataType: "script",
      data: {category_id: $("#job_category_id").val()},
      error: function(){
      },
      success: function(){
        $("#job_skill_needs_").multipleSelect("enable");
      }
    })
  });

  $("#job_country_id").change(function(event) {

    console.log('test');
    /* Act on the event */
    $.ajax({
      url: "/jobs/regions",
      method: "GET",
      dataType: "script",
      data: {country_id: $("#job_country_id").val()},
      error: function(){
      },
      success: function(){
        console.log("success")
      }
    })
  });

  $("#region").change(function(event) {

    console.log('test');
    /* Act on the event */
    $.ajax({
      url: "/jobs/cities",
      method: "GET",
      dataType: "script",
      data: {region_id: $("#region").val()},
      error: function(){
      },
      success: function(){
        console.log("success")
      }
    })
  });
});
  