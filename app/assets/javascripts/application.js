// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require jquery-fileupload
//= require jquery-fileupload/basic-plus
//= require video
//= require bootstrap-wysihtml5
//= require jquery
//= require fancybox
//= require_tree .

$(document).on('page:load', function(){
  window['rangy'].initialized = false
});
$(document).on('turbolinks:load', function() {
  $("img.lazy").lazyload();

});
$(document).ready(function() {
  $(".multiple-select").multipleSelect({
    filter: true,
    multiple: true,
    placeholder: "Select Skill Needed",
    width: '100%',
  });
  $("a.fancybox").fancybox({
    padding    : 0,
    margin     : 5,
    nextEffect : 'fade',
    prevEffect : 'none',
    autoCenter : false,
    afterLoad  : function () {
      $.extend(this, {
          aspectRatio : false,
          type    : 'html',
          width   : '100%',
          height  : '100%',
          content : '<div class="fancybox-image" style="background-image:url(' + this.href + '); background-size: cover; background-position:50% 50%;background-repeat:no-repeat;height:100%;width:100%;" /></div>'
      });
    }
  });
});
$(function(){
   if ($('#ms-menu-trigger')[0]) {
        $('body').on('click', '#ms-menu-trigger', function() {
            $('.ms-menu').toggleClass('toggled'); 
        });
    }
});
