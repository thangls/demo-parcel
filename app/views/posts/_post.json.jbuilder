json.extract! post, :id, :content, :url_extract, :likes_count, :comments_count, :created_at, :updated_at
json.url post_url(post, format: :json)