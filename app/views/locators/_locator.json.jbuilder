json.extract! locator, :id, :created_at, :updated_at
json.url locator_url(locator, format: :json)