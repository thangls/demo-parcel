json.extract! parcel, :id, :created_at, :updated_at
json.url parcel_url(parcel, format: :json)