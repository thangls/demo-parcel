json.extract! admin_parcel, :id, :created_at, :updated_at
json.url admin_parcel_url(admin_parcel, format: :json)