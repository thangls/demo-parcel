module JobService
  class JobCreator
    extend ActiveModel::Naming

    attr_reader :job
    attr_accessor :errors

    def initialize(params)
      @params = params
      @errors = ActiveModel::Errors.new(self)
    end

    def create
      create_job!
      true
    rescue ActiveRecord::RecordInvalid => e
      @errors << e.record.errors
    end

    private
    def create_job!
      @job = Job.create!(title: title, description: description)
    end
  end
end
