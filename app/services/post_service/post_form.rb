module PostService
  class PostForm < SkillTracker::Command

    attr_reader :params
    def initialize(params={})
      super()
      @params = params
    end

    def call
      do_something
    rescue ActiveRecord::RecordInvalid => e
      @errors = e.record.errors
    end

    private
    def do_something
      Job.create!(params)
    end
  end
end
