module SkillTracker
  class Command
    extend ActiveModel::Naming
    attr_reader :errors

    def initialize
      @errors = ActiveModel::Errors.new(self)
    end

    def transaction(&block)
      ActiveRecord::Base.transaction(&block) if block_given?
    end

    def read_attribute_for_validation(attr)
      send(attr)
    end

    class << self
      def human_attribute_name(attr, options = {})
        attr
      end

      def lookup_ancestors
        [self]
      end
    end
  end
end
