class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    @post = Post.new
    @posts = Post.all.order(created_at: :desc)
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  def upload_images
    image_stream = params[:post][:images][0]
    image = Image.new({image: image_stream})
    image.save
    render json: image.to_json, status: :ok
    # images_params.each do |image_param|
    #   p image_param
    #   image = Image.new(image_param)
    #   if @image.save
    #     render json: @image.to_json, status: :ok
    #   else
    #     render json: @image.errors, status: :unprocessable_entity
    #   end
    # end
  end
  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    def images_params
      params.require(:post).permit(images: [])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params[:post][:url_extract] ={}
      params.require(:post).permit(:content, :url_extract, :likes_count, :comments_count, image_ids: [])
    end
end
