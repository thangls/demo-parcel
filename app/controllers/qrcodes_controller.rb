class QrcodesController < ApplicationController
  before_action :set_qrcode, only: [:show, :edit, :update, :destroy]

  # GET /qrcodes
  # GET /qrcodes.json
  def index
    @qrcodes = Qrcode.all
  end

  # GET /qrcodes/1
  # GET /qrcodes/1.json
  def show
  end

  # GET /qrcodes/new
  def new
    @qrcode = Qrcode.new
  end

  # GET /qrcodes/1/edit
  def edit
  end

  # POST /qrcodes
  # POST /qrcodes.json
  def create
    @qrcode = Qrcode.new(qrcode_params)
    qrcode = RQRCode::QRCode.new(@qrcode.data)
    png = qrcode.as_png(
          resize_gte_to: false,
          resize_exactly_to: false,
          fill: 'white',
          color: 'black',
          size: 120,
          border_modules: 4,
          module_px_size: 6,
          file: nil # path to write
          )
    @qrcode.qr_img_url = png.to_data_url
    respond_to do |format|
      if @qrcode.save
        format.html { redirect_to @qrcode, notice: 'Qrcode was successfully created.' }
        format.json { render :show, status: :created, location: @qrcode }
      else
        format.html { render :new }
        format.json { render json: @qrcode.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /qrcodes/1
  # PATCH/PUT /qrcodes/1.json
  def update
    respond_to do |format|
      if @qrcode.update(qrcode_params)
        format.html { redirect_to @qrcode, notice: 'Qrcode was successfully updated.' }
        format.json { render :show, status: :ok, location: @qrcode }
      else
        format.html { render :edit }
        format.json { render json: @qrcode.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qrcodes/1
  # DELETE /qrcodes/1.json
  def destroy
    @qrcode.destroy
    respond_to do |format|
      format.html { redirect_to qrcodes_url, notice: 'Qrcode was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_qrcode
      @qrcode = Qrcode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def qrcode_params
      params.require(:qrcode).permit(:data)
    end
end
