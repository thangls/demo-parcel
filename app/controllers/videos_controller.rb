class VideosController < ApplicationController

	def index
		@image = Image.new
	end

	def new
		@video = Video.new
	end

	def show
		@video = Video.find(params[:id])
	end

	def create
		@video = Video.new(video_params)
		if @video.save
			redirect_to @video, notice: "Video was created successfully!"
		else
			render :new
		end
	end

	def video_params
		params.require(:video).permit(:title, :video)
	end
end
