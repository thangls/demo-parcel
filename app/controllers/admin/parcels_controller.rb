class Admin::ParcelsController < Admin::BaseController
  before_action :set_admin_parcel, only: [:show, :edit, :update, :destroy]

  # GET /admin/parcels
  # GET /admin/parcels.json
  def index
    @parcels = Parcel.all
  end

  # GET /admin/parcels/1
  # GET /admin/parcels/1.json
  def show
  end

  # GET /admin/parcels/new
  def new
    @parcel = Parcel.new
  end

  # GET /admin/parcels/1/edit
  def edit
  end

  # POST /admin/parcels
  # POST /admin/parcels.json
  def create
    @parcel = Parcel.new(parcel_params)

    respond_to do |format|
      if @parcel.save
        format.html { redirect_to @parcel, notice: 'Parcel was successfully created.' }
        format.json { render :show, status: :created, location: @parcel }
      else
        format.html { render :new }
        format.json { render json: @parcel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/parcels/1
  # PATCH/PUT /admin/parcels/1.json
  def update
    respond_to do |format|
      if @parcel.update(parcel_params)
        format.html { redirect_to @parcel, notice: 'Parcel was successfully updated.' }
        format.json { render :show, status: :ok, location: @parcel }
      else
        format.html { render :edit }
        format.json { render json: @parcel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/parcels/1
  # DELETE /admin/parcels/1.json
  def destroy
    @parcel.destroy
    respond_to do |format|
      format.html { redirect_to admin_parcels_url, notice: 'Parcel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_parcel
      @parcel = Parcel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def parcel_params
      params.fetch(:parcel, {})
    end
end
