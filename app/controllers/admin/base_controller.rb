class Admin::BaseController < ApplicationController
	before_action :authenticate_user!
	layout 'admin/application'

	def after_sign_in_path_for(resource)
		p "================"
		p session[:user_return_to]
	end
end
