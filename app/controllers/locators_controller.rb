class LocatorsController < ApplicationController
  before_action :set_locator, only: [:show, :edit, :update, :destroy]

  # GET /locators
  # GET /locators.json
  def index
    @locators = Locator.all
  end

  # GET /locators/1
  # GET /locators/1.json
  def show
  end

  # GET /locators/new
  def new
    @locator = Locator.new
  end

  # GET /locators/1/edit
  def edit
  end

  # POST /locators
  # POST /locators.json
  def create
    @locator = Locator.new(locator_params)

    respond_to do |format|
      if @locator.save
        format.html { redirect_to @locator, notice: 'Locator was successfully created.' }
        format.json { render :show, status: :created, location: @locator }
      else
        format.html { render :new }
        format.json { render json: @locator.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /locators/1
  # PATCH/PUT /locators/1.json
  def update
    respond_to do |format|
      if @locator.update(locator_params)
        format.html { redirect_to @locator, notice: 'Locator was successfully updated.' }
        format.json { render :show, status: :ok, location: @locator }
      else
        format.html { render :edit }
        format.json { render json: @locator.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /locators/1
  # DELETE /locators/1.json
  def destroy
    @locator.destroy
    respond_to do |format|
      format.html { redirect_to locators_url, notice: 'Locator was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_locator
      @locator = Locator.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def locator_params
      params.require(:locator).permit(:name, :lat, :lng)
    end
end
