class JobsController < ApplicationController
  before_action :set_job, only: [:show, :edit, :update, :destroy]
  # GET /jobs
  # GET /jobs.json
  def index
    @jobs = Job.all
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
  end

  # GET /jobs/new
  def new
    @job = Job.new
    @processes = Iprocess.where(category_id: @job.category_id)
  end

  # GET /jobs/1/edit
  def edit
    @processes = Iprocess.where(category_id: @job.category_id)
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)

    respond_to do |format|
      if @job.save
        format.html { redirect_to @job, notice: 'Job was successfully created.' }
        format.json { render :show, status: :created, location: @job }
      else
        format.html { render :new }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    respond_to do |format|
      if @job.update(job_params)
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { render :show, status: :ok, location: @job }
      else
        format.html { render :edit }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job.destroy
    respond_to do |format|
      format.html { redirect_to jobs_url, notice: 'Job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def get_processes
    p params[:category_id]
    @processes = Iprocess.where(category_id: params[:category_id])
  end

  def regions
    p params[:country_id]
    @regions = Region.where(country_id: params[:country_id])
  end

  def cities
    p params[:region_id]
    @cities = City.where(region_id: params[:region_id])
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params[:job][:skill_needs] = params[:job][:skill_needs].present? ? params[:job][:skill_needs] : []
      params.require(:job).permit(:title, :description, :lat, :lng, :category_id, skill_needs: [])
    end
end
