class LinkPreviewController < ApplicationController
  def index
    object = LinkThumbnailer.generate(params[:url])
    render json: object.to_json
  end
end
