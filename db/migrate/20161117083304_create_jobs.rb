class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.text :description
      t.decimal :lat
      t.decimal :lng
      t.timestamps null: false
    end
  end
end
