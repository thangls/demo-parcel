class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :content
      t.json :url_extract
      t.integer :likes_count
      t.integer :comments_count

      t.timestamps null: false
    end
  end
end
