class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
    	t.belongs_to :country
    	t.belongs_to :region
    	t.decimal :latitude
    	t.decimal :longitude
    	t.string :name
      t.timestamps null: true
    end
  end
end
