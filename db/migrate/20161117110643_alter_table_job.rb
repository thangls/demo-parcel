class AlterTableJob < ActiveRecord::Migration
  def change
    # change_column :jobs, :skills, :integer, array: true
    change_table :jobs do |t|
      t.integer :skill_needs, array: true
    end
    add_index :jobs, :skill_needs, using: "gin"
  end
end
