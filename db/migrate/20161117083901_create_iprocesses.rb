class CreateIprocesses < ActiveRecord::Migration
  def change
    create_table :iprocesses do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
