class CreateSkillsUsers < ActiveRecord::Migration
  def change
    create_table :skills_users do |t|
      t.belongs_to :user
      t.belongs_to :skill
      t.timestamps null: false
    end
  end
end
