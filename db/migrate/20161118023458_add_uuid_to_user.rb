class AddUuidToUser < ActiveRecord::Migration
  def change
    enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')
    add_column :users, :uuid, :uuid, default: 'gen_random_uuid()'

    change_table :users do |t|
      t.remove :id
      t.rename :uuid, :id
    end
    execute "ALTER TABLE users add primary key (id);"
  end
end
