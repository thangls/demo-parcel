class CreateParcels < ActiveRecord::Migration
  def change
    create_table :parcels do |t|
    	t.string :name
    	t.string :size
    	t.string :weight
    	t.string :total_fee
    	t.integer :status
      t.timestamps null: false
    end
  end
end
