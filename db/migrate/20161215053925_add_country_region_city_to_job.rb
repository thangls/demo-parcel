class AddCountryRegionCityToJob < ActiveRecord::Migration
  def change
  	change_table :jobs do |t|
  		t.belongs_to :country
  		t.belongs_to :region
  		t.belongs_to :city
  	end
  end
end
