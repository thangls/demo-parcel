class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.attachment :image
      t.string :title
      t.belongs_to :user
      t.timestamps null: false
    end
  end
end
