class CreateQrcodes < ActiveRecord::Migration
  def change
    create_table :qrcodes do |t|
      t.string :data
      t.string :qr_img_url

      t.timestamps null: false
    end
  end
end
