class CreateOrganisations < ActiveRecord::Migration
  def change
    create_table :organisations do |t|
      t.string :name
      t.string :email
      t.attachment :cover
      t.attachment :logo
      t.integer :owner_id
      t.timestamps null: false
    end
  end
end
