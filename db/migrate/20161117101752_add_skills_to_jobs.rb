class AddSkillsToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :skills, :string, array: true, default: []
  end
end
