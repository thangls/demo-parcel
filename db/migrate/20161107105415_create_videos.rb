class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
    	t.attachment :video
    	t.string :title
      t.timestamps null: false
    end
  end
end
