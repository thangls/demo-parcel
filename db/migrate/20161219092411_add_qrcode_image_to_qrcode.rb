class AddQrcodeImageToQrcode < ActiveRecord::Migration
  def change
  	change_table :qrcodes do |t|
  		t.attachment :qrcode
  	end
  end
end
