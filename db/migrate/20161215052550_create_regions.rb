class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
    	t.integer :country_id
    	t.string :name
    	t.string :code
      t.timestamps null: true
    end
  end
end
