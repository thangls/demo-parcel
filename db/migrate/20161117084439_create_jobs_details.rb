class CreateJobsDetails < ActiveRecord::Migration
  def change
    create_table :jobs_details do |t|
      t.belongs_to :iprocess
      t.belongs_to :skill
      t.belongs_to :job
      t.timestamps null: false
    end
  end
end
