# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# User.create(email: "admin@example.com", password: "123456789")
iprocess = Iprocess.find_or_create_by(name: "Job Type")
sub_process = [
	"Welder",
	"Boilermaker",
	"Pipe Fitter",
	"Poly Welder"
]
Skill.create!(sub_process.map{|s| {iprocess: iprocess, name: s}})

iprocess = Iprocess.create!(name: "Welding Process")
sub_process = [
	"MMAW",
	"FCAW",
	"GTAW",
	"SAW",
	"GMAW",
	"OAW"
]
Skill.create(sub_process.map {|n| {iprocess: iprocess, name: n}})

iprocess = Iprocess.create!( name: "Welding Position")
sub_process = [
	"PAD WELD - DOWN",
	"PAD WELD - HORIZONTAL",
	"1F",
	"2F",
	"1G"
]
Skill.create!(sub_process.map {|m| {iprocess: iprocess, name: m}})


iprocess = Iprocess.create!(name: "Material")
Skill.create(name: "PLATE", iprocess: iprocess)

iprocess = Iprocess.create!(name: "Material Type")
sub_process = [
	"Stainiess Steel Grade - 308",
	"Stainiess Steel Grade - 316",
	"Stainiess Steel Grade - 317",
	"Stainiess Steel Grade - 320",
	"Stainiess Steel Grade - 347",
	"Stainiess Steel Grade - OTHER",
	"Carbon Steel Grade - 7-460",
	"Carbon Steel Grade - 5-490",
	"Carbon Steel Grade - 7-490",
	"Carbon Steel Grade - Rail",
	"Carbon Steel Grade - Lloyds Grade A",
	"Carbon Steel Grade - Lloyds Grade B",
	"Carbon Steel Grade - Lloyds Grade D",
	"Carbon Steel Grade - Lloyds Grade E",
	"Carbon Steel Grade - ABS Class B",
	"Carbon Steel Grade - ABS Class CS",
	"Carbon Steel Grade - OTHER",
	"BisAlloy",
	"OTHER"
]
