require 'test_helper'

class Admin::ParcelsControllerTest < ActionController::TestCase
  setup do
    @admin_parcel = admin_parcels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_parcels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_parcel" do
    assert_difference('Admin::Parcel.count') do
      post :create, admin_parcel: {  }
    end

    assert_redirected_to admin_parcel_path(assigns(:admin_parcel))
  end

  test "should show admin_parcel" do
    get :show, id: @admin_parcel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_parcel
    assert_response :success
  end

  test "should update admin_parcel" do
    patch :update, id: @admin_parcel, admin_parcel: {  }
    assert_redirected_to admin_parcel_path(assigns(:admin_parcel))
  end

  test "should destroy admin_parcel" do
    assert_difference('Admin::Parcel.count', -1) do
      delete :destroy, id: @admin_parcel
    end

    assert_redirected_to admin_parcels_path
  end
end
